# Run the book pipeline.
#
# Usage:
#
#   make
#			Makes all formats
#
#   make [FORMAT]
#			Make the specific format
#
# Supported formats:
#		- html
#		- tex
#		- pdf
#		- epub
#		- md
#		- mobi
#
# See also:
#   - http://mrbook.org/blog/tutorials/make/

all:	_md _html _odt _pdf _tex _epub _mobi
html:	_md _html
odt:	_md _odt
pdf:	_md _pdf
tex:	_md _tex
epub:	_md _epub
mobi:	_md _epub _mobi

# Never run these directly.
_md:
	pp -en src/*.md src/metadata.yml > dist/book.md

_html:
	pandoc dist/book.md --from=markdown --self-contained --template=templates/template-custom.html --to=html --output=dist/book.html --resource-path=src

_odt:
	pandoc dist/book.md --from=markdown --self-contained --to=odt --output=dist/book.odt --resource-path=src

_tex:
	pandoc dist/book.md --from=markdown --self-contained --template=templates/template-custom.latex --to=latex --output=dist/book.tex --resource-path=src

_pdf:
	pandoc dist/book.md --from=markdown --self-contained --template=templates/template-custom.latex --to=latex --output=dist/book.pdf --resource-path=src

_epub:
	pandoc dist/book.md --from=markdown --self-contained --template=templates/template-custom.epub --to=epub --output=dist/book.epub --resource-path=src

_mobi:	_epub
	kindlegen dist/book.epub

.PHONY: all


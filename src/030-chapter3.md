## Chapter 3: About the Preprocessing

### Overview

See the README.md for more details. In short, these pages are built by writing the book in Github Flavored Markdown, using `pp` to convert to a normalized markdown, then using `pandoc` to convert to all the formats. KindleGen is then used to convert an `epub` to a `mobi` file for Amazon store.

### What you can write in this book

There are many features to this pipeline. Anything that `pp` supports, you can use.

`pp` implements:

- macros
- literate programming
- GraphViz, PlantUML and ditaa diagrams
- Asymptote and R figures
- Bash, Cmd, PowerShell, Python, Lua, Haskell and R scripts
- Mustache

### Directory structure

In order to get pages to print in order, you'll need to name your files in the order of how the pages are displayed. Because of this, it's recommended to:

- Each file gets a 3 digit prefix
- The prefix increases by 10 for each sequential file
- Each file represents a chapter

For example, files would be named `010-chapter1.md`, `020-chapter2.md`, etc.


# Markdown Book Pipeline

This book was printed using [https://gitlab.com/josephdpurcell/markdown-book-pipeline](https://gitlab.com/josephdpurcell/markdown-book-pipeline).

The first two chapters are written by The Brothers Grimm, copied from Project Guttenberg [https://www.gutenberg.org/files/2591/2591-h/2591-h.htm](https://www.gutenberg.org/files/2591/2591-h/2591-h.htm).

The concept of the book pipeline is from Thorsten Ball's blog post [The Tools I Use To Write Books](https://thorstenball.com/blog/2018/09/04/the-tools-i-use-to-write-books/).

\tableofcontents


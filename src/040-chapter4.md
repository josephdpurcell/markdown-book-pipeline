## Chapter 4: Examples

Let's look at some examples of what you can write.

### Expressions

The `if` and `eval` macros take an expression and evaluate it. Expressions are made of:

- integers
- string (`"..."`)
- integer operators (`+`, `-`, `*`, `/`)
- boolean operators (`!`, `not`, `&&`, `and`, `||`, `or`, `xor`)
- relational operators (`==`, `/=`, `!=`, `<`, `<=`, `>`, `>=`)
- parentheses, brackets and braces

Boolean values are coded as integers or string (`0` and `""` are false, other values are true).

Macros can be called in expressions. They are preprocessed before evaluating the expression.

For example:

```
    !raw(!if(2==2)(Text if expression is TRUE)(Text if expression is FALSE))
```

Will print out:

```
    !if(2==2)(Text if expression is TRUE)(Text if expression is FALSE)
```

### Literate programming example

Some code that can be highlighted:

!lit(@file.php)(php)(<?php

echo "Hello world!";
)

To see all available languages, run: `pandoc --list-highlight-languages`

### Variable Replacement with Mustache

The following mustache template like so:

> ```
!raw(!mustache(example-variables.yml)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
> This a copy of the documentation for `{{name}}` version {{version}} by {{author}}.
> 
> Copyright **{{copyright}}**.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~)
> ```

Will print this:

> ```
!mustache(src/example-variables.yml)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
> This a copy of the documentation for `{{name}}` version {{version}} by {{author}}.
>
> Copyright **{{copyright}}**.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
> ```

Using the contents of example-variables.yml:

!lit(@example-variables.yml)(yaml)(name:                pp
version:             2.7
github:              "CDSoft/pp"
license:             GPL-3
author:              "Christophe Delord"
maintainer:          "cdsoft.fr"
copyright:           "2015, 2016, 2017, 2018 Christophe Delord")

### Table

!csv(src/example-table.csv)

### Bash

The following:

> ```
!raw(!bash
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
> echo "Hi, I'm $SHELL $BASH_VERSION"
> RANDOM=42 # seed
> echo "Here are a few random numbers: $RANDOM, $RANDOM, $RANDOM"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~)
> ```

Will print:

> ```
!bash
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
echo "Hi, I'm $SHELL $BASH_VERSION"
RANDOM=42 # seed
echo "Here are a few random numbers: $RANDOM, $RANDOM, $RANDOM"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
> ```

### Images

You can include images like so:

![A 480x250 image](480x250.png)

### LaTeX

You can also put in LaTeX code directly. This is extremely powerful and as you try to make your book customized and want to place specific text or assets about the page you will need to leverage this.

For example, using `\tableofcontents` will render a table of contents as you see earlier in this book.

You can do things like alignment and centering; the following code:

> ```
!raw(\mbox{}\hfill Some text on the right)
```

Will print:

\mbox{}\hfill Some text on the right

**IMPORTANT!** You'll see that the text aligns to the right in a PDF but not in any other format.

Refer to LaTeX documentation for more examples.


# Markdown Book Pipeline

Inspired by https://thorstenball.com/blog/2018/09/04/the-tools-i-use-to-write-books/, this repo is intended to show how to build a book pipeline with Markdown as the source.

## Installation

1. Install stack: https://docs.haskellstack.org/en/stable/install_and_upgrade/, which recommends:
    * `wget -qO- https://get.haskellstack.org/ | sh`
2. Install latest pandoc from https://github.com/jgm/pandoc/releases/ (note: apt-get version is out of date):
    * `wget https://github.com/jgm/pandoc/releases/download/2.3.1/pandoc-2.3.1-1-amd64.deb`
    * `sudo dpkg -i pandoc-2.3.1-1-amd64.deb`
3. Install pdflatex:
    * `sudo apt-get install texlive-latex-base`
    * `sudo apt-get install texlive-fonts-recommended`
    * `sudo apt-get install texlive-fonts-extra`
    * `sudo apt-get install texlive-latex-extra`
4. Download pp: https://cdsoft.fr/pp/, which is downloading https://cdsoft.fr/pp/pp.tgz and running `tar xzvf pp.tgz` in a directory
5. Remove this line from `doc/pp.md`
    * `95     - Fedora §sh[(cat /etc/redhat-release | tr -d -c "[0-9]") || true] (64 bit): <https://cdsoft.fr/pp/pp-linux-x86_64.txz>`
6. Remove a all the stuff that doesn't workin that `doc/pp.md` file
7. Run `make`
8. Add `pp` to global path:
    * `sudo cp ./.stack-work/install/x86_64-linux/lts-12.7/8.4.3/bin/pp /usr/local/bin/pp`
9. Install kindlegen: https://www.amazon.com/gp/feature.html?ie=UTF8&docId=1000765211
    * `mkdir kindlegen`
    * `wget http://kindlegen.s3.amazonaws.com/kindlegen_linux_2.6_i386_v2_9.tar.gz`
    * `tar xzvf kindlegen_linux_2.6_i386_v2_9.tar.gz`
    * `chmod +x kindlegen`
    * `sudo mv kindlegen /usr/local/bin/`
10. Optionally install Calibre (or another epub reader) to view epubs:
	* `sudo apt-get install calibre`

## TODO

- Figure out how to customize odt.
- Figure out why all the stuff that doesn't work in the pp.md file on install.
- Consider using "book" as the documentclass for PDF.

## Pipeline

### 1. Preprocess the markdown with pp

```
pp -en src/* > dist/book.md
```

### 2. Publish in epub and other formats using pandoc

```
pandoc dist/book.md --from=markdown --self-contained --template=templates/template-custom.html --to=html --output=dist/book.html
pandoc dist/book.md --from=markdown --self-contained --template=templates/template-custom.epub --to=epub --output=dist/book.epub
pandoc dist/book.md --from=markdown --self-contained --template=templates/template-custom.latex --to=latex --output=dist/book.pdf
```

Note: epub format is required for step 3.

### 3. Publish in mobi (kindle) format using kindlegen

```
kindlegen dist/book.epub
```

## Getting Started

See the `makefile` for an easy way to generate files for a book. It takes the files from `src/*` and generates output into `dist/*`.

To run it and see all of this in action:

```
git clone git@gitlab.com:josephdpurcell/markdown-book-pipeline.git
cd markdown-book-pipeline/
make
```

Then, look in the `dist/` folder for the published files.

To publish just a specific format, run:

```
make pdf
```

Where pdf is the format you want to publish in.

Note: recall that `make` is just a task runner; read the file and customize it as you wish.

## Customizing

Most importantly, you'll want to customize the files in `templates/` to generate a book to your liking. This will require solid GitHub Flavored Markdown, LaTeX, and `pp` knowledge based on how much you want to customize the look and feel of the book.

**IMPORTANT!** Customizations you make that target a specific output (e.g. using LaTeX commands in your source) may not render correctly in other formats.

## Further Reading

* https://github.github.com/gfm/ - GitHub Flavored Markdown documentation
* https://www.latex-project.org/help/documentation/ - LaTeX documentation
* https://uoftcoders.github.io/studyGroup/lessons/misc/pandoc-intro/lesson/ - Why use pandoc and how to use templates
* https://github.com/kjhealy/pandoc-templates - Some more custom templates
* https://github.com/jgm/pandoc/wiki/User-contributed-templates - More templates

